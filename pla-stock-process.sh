#!/bin/sh
cd /home/papadopoulospk/pla-stock-feed/apache-beam-demo &&
python /home/papadopoulospk/pla-stock-feed/apache-beam-demo/ftpTransfer.py &&
python /home/papadopoulospk/pla-stock-feed/apache-beam-demo/main.py &&
gsutil cat gs://feeddata-test-konos-1/merchcenter/files/output-* > /home/papadopoulospk/pla-stock-feed/apache-beam-demo/final-data.csv &&
cat /home/papadopoulospk/pla-stock-feed/apache-beam-demo/header.csv /home/papadopoulospk/pla-stock-feed/apache-beam-demo/final-data.csv > /home/papadopoulospk/pla-stock-feed/apache-beam-demo/final.csv &&
rm /home/papadopoulospk/pla-stock-feed/apache-beam-demo/final-data.csv &&
gsutil mv /home/papadopoulospk/pla-stock-feed/apache-beam-demo/final.csv gs://feeddata-test-konos-1/merchcenter/skus-stock.csv &&
gsutil mv gs://feeddata-test-konos-1/merchcenter/files gs://feeddata-test-konos-1/merchcenter/log &&
# gsutil cp gs://feeddata-test-konos-1/merchcenter/skus-stock.csv gs://merchantcenter6242457/skus-stock.csv &&
gsutil rm gs://feeddata-test-konos-1/uploads/*