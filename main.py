from __future__ import absolute_import

import logging

import calendar
import time
import datetime

import pdb

import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions

logging.getLogger().setLevel(logging.INFO)
# currentTime = calendar.timegm(time.gmtime())
now = datetime.datetime.now()
currentTime = now.hour
currentDate = now.day
currentMonth = now.month
currentTimestamp = str(int(round(time.time())))

pipeline_options = PipelineOptions([
      # CHANGE 2/5: (OPTIONAL) Change this to DataflowRunner to
      # run your pipeline on the Google Cloud Dataflow Service.
      # '--runner=DirectRunner',
      '--runner=DataflowRunner',
      # CHANGE 3/5: Your project ID is required in order to run your pipeline on
      # the Google Cloud Dataflow Service.
      '--project=test-r-big-query',
      # CHANGE 4/5: Your Google Cloud Storage path is required for staging local
      # files.
      '--staging_location=gs://feeddata-test-konos-1/staging/',
      # CHANGE 5/5: Your Google Cloud Storage path is required for temporary
      # files.
      '--temp_location=gs://feeddata-test-konos-1/temp/',
      '--job_name=pla-stock-processing-' + str(currentTime) + '-' + str(currentDate) + '-' + str(currentMonth) ,
  ])
pipeline_options.view_as(SetupOptions).save_main_session = True
# The DoFn to perform on each element in the input PCollection.


class filterProductViews(beam.DoFn):
    def process(self, element):

        columns = element.split("\t")
        if ((columns[0] == 'Product Detail') and (columns[5] == 'www.debenhams.com' or columns[5] == 'm.debenhams.com')):
            yield columns

class filterTransactions(beam.DoFn):
    def process(self, element):

        columns = element.split("\t")
        if (columns[0] == 'Purchase'):
            yield columns

class processTransactions(beam.DoFn):
    def process(self, element):
        transaction_id = element[3]
        products_string = element[1]
        products_array = products_string.split(',')
        result = []
        for product in products_array:
            splitted_product = product.split(';')
            entries = len(splitted_product)
            if (entries >= 2):
                product_id = splitted_product[1]
            else:
                product_id = ''
            
            if (entries >= 3):
                product_units = splitted_product[2]
            else:
                product_units = 0
                
            if (entries >= 4):
                total_price = splitted_product[3]
            else:
                total_price = 0.00

            if (entries >= 5):
                events = splitted_product[4]
                for event in events.split('|'):
                    order_discount = 0.00
                    if (event.lower().find('236=') > -1):
                        order_discount = event.split('=')[1]
                    order_discount = order_discount
            else:
                order_discount = 0.00

            if (entries >= 6):
                # pdb.set_trace()
                evars = splitted_product[5]
                sku = ''
                for evar in evars.split('|'):
                    if (evar.lower().find('evar33=') > -1):
                        sku = evar.split('=')[1]
                    product_sku = sku
            else:
                product_sku = ''
            
            if ((product_id != '') & (product_id.lower()!= 'shipping')):
                result.append([str(transaction_id), str(product_id), str(product_sku), str(product_units), str(total_price), str(order_discount)])
        return result

class splitProductEntriesIntoLine(beam.DoFn):
    def process(self, element):
        return element[1].split(',')

class filterPDPData(beam.DoFn):
    def process(self, element):
        if (element != ""):
            # Parse Product ID, product SKU, stock on PDP Level
            product_string = element
            product_array = product_string.split(";")
            if len(product_array) >= 5:
                events = product_array[4].split('|')
            else:
                events = []

            if len(product_array) >= 6:
                evars = product_array[5].split('|')
            else:
                evars = []

            if len(product_array) >= 1:
                product_id = product_array[1]
            else:
                product_id = ''

            final_evar = ""
            final_event = ""
            final_product_id = product_id

            for event in events:
                if (event.lower().find('291=')>-1):
                    final_event = event

            for evar in evars:
                if (evar.lower().find('evar33')>-1):
                    final_evar = evar

            if ((final_evar != "") & (final_event != "")):
                final_evar = final_evar.split("=")[1]
                final_event = final_event.split("=")[1]
                # Format : SKU, Stock Level, Hit Timestamp
                yield final_product_id, final_evar, float(final_event)


class extractSkuAndStock(beam.DoFn):
    def process(self, element):
        # input of element: product ID, product SKU, stock
        yield element[1], element[2]

class FindMinVolume(beam.DoFn):
    def process(self, element):
        min_stock = min(element[1])
        sku = element[0]
        yield sku , min_stock

class FinalFormat(beam.DoFn):
    def process(self, element):
        #pdb.set_trace()
        product_sku = str(element[0])
        product_sku_stock = element[1]
        if (product_sku_stock <= 0):
            yield (product_sku + ',out of stock')

class prepareBQProductIdSkuMapping(beam.DoFn):
    def process(self, element):
        # input of element: product ID, product SKU, stock
        product_id = str(element[0])
        product_sku = str(element[1])
        yield product_id  + ',' + product_sku

class prepareBQProductSkuStock(beam.DoFn):
    def process(self, element):
        min_stock = str(int(element[1]))
        productSku = str(element[0])
        yield (currentTimestamp + ',' + productSku + ',' + min_stock)

class formatTransactions(beam.DoFn):
    def process(self, element):
        data = [currentTimestamp]
        data = data + element
        yield ','.join(data)

p = beam.Pipeline(options=pipeline_options)

# Apply a ParDo to the PCollection "words" to compute lengths for each word.
data = p | 'Load Data' >> beam.io.ReadFromText('gs://feeddata-test-konos-1/uploads/*.tsv.gz')
# data = p | 'Load Data' >> beam.io.ReadFromText('AdobeFeeds/*.tsv.gz')
# data = p | beam.io.ReadFromText('data-small.csv')

data_filtered = data | 'Filter Product lines from DOTCOM only' >> beam.ParDo(filterProductViews())
data_filtered = data_filtered | 'Split product-sku entries into multiple lines' >> beam.ParDo(splitProductEntriesIntoLine())
data_filtered = data_filtered | 'Extract Product ID, SKU ID and Stock volume' >> beam.ParDo(filterPDPData())
data_filtered_skus_and_stock = data_filtered | 'Filter Product SKU and Sku Stock' >> beam.ParDo(extractSkuAndStock())
data_filtered_skus_and_stock = data_filtered_skus_and_stock | 'Group by SKU' >> beam.GroupByKey()
data_filtered_skus_and_stock = data_filtered_skus_and_stock | 'Obtain min stock value' >> beam.ParDo(FindMinVolume())

# Part prepared for Big Query Storage
data_filtered_write_to_bq_product_id_sku = data_filtered | 'Format BQ output - Product ID and SKUs' >> beam.ParDo(prepareBQProductIdSkuMapping())
data_filtered_write_to_bq_product_id_sku = data_filtered_write_to_bq_product_id_sku | "Remove duplicates" >> beam.RemoveDuplicates()
output_res = data_filtered_write_to_bq_product_id_sku | 'BQ Output - Product ID and SKUs Mapping' >> beam.io.WriteToText('gs://feeddata-test-konos-1/bq/mapping/product-id-sku-output-' + str(currentMonth) + '-' + str(currentDate) + '-' + str(currentTime) + '.csv')
# output_res = data_filtered_write_to_bq_product_id_sku | "Dummy Local output 2" >> beam.io.WriteToText('output/output-bq-2-' + str(currentTime) + '.txt')

data_filtered_write_to_bq_product_sku_stock = data_filtered_skus_and_stock | 'Format BQ output - Product SKUs and Stock' >> beam.ParDo(prepareBQProductSkuStock())
output_res = data_filtered_write_to_bq_product_sku_stock | 'BQ Output - Product SKUs and Stock' >> beam.io.WriteToText('gs://feeddata-test-konos-1/bq/stock-volumes/products-sku-stock-output-' + str(currentMonth) + '-' + str(currentDate) + '-' + str(currentTime) + '.csv')
# data_calc = data_filtered_write_to_bq_product_sku_stock | "Dummy Local output 3" >> beam.io.WriteToText('output/output-bq-3-' + str(currentTime) + '.txt')

# PLA Stock file format
data_filtered_write_to_file = data_filtered_skus_and_stock | "Final Format" >> beam.ParDo(FinalFormat())
# data_calc = data_filtered_write_to_file | "Dummy Local output" >> beam.io.WriteToText('output/output-' + str(currentTime) + '.txt')
data_calc = data_filtered_write_to_file | 'Write to Merchant Center output' >> beam.io.WriteToText('gs://feeddata-test-konos-1/merchcenter/files/output-' + str(currentMonth) + '-' + str(currentDate) + '-' + str(currentTime) + '.csv')


# Generate Transactions Entries
data_transactions = data | 'Filter Transactions' >> beam.ParDo(filterTransactions())
data_transactions = data_transactions | 'Split Transactions into multiple entries' >> beam.ParDo(processTransactions())
data_transactions = data_transactions | 'Format Transctions data' >> beam.ParDo(formatTransactions())
# data_calc = data_transactions | "Dummy Local output 4" >> beam.io.WriteToText('output/output-4-' + str(currentTime) + '.txt')
data_calc = data_transactions | 'BQ Output - Transactions' >> beam.io.WriteToText('gs://feeddata-test-konos-1/bq/transactions/product-purchases-' + str(currentMonth) + '-' + str(currentDate) + '-' + str(currentTime) + '.csv')

result = p.run()
result.wait_until_finish()