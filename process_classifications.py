from __future__ import absolute_import
import logging
import datetime
import pdb
import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions

logging.getLogger().setLevel(logging.INFO)
# currentTime = calendar.timegm(time.gmtime())
now = datetime.datetime.now()
currentTime = now.hour
currentDate = now.day

pipeline_options = PipelineOptions([
      # CHANGE 2/5: (OPTIONAL) Change this to DataflowRunner to
      # run your pipeline on the Google Cloud Dataflow Service.
      # '--runner=DirectRunner',
      '--runner=DataflowRunner',
      # CHANGE 3/5: Your project ID is required in order to run your pipeline on
      # the Google Cloud Dataflow Service.
      '--project=test-r-big-query',
      # CHANGE 4/5: Your Google Cloud Storage path is required for staging local
      # files.
      '--staging_location=gs://feeddata-test-konos-1/staging/',
      # CHANGE 5/5: Your Google Cloud Storage path is required for temporary
      # files.
      '--temp_location=gs://feeddata-test-konos-1/temp/',
      '--job_name=product-classification-processing-' + str(currentDate) + '-' + str(currentTime),
  ])
pipeline_options.view_as(SetupOptions).save_main_session = True
# The DoFn to perform on each element in the input PCollection.


class filterLines(beam.DoFn):
    def process(self, element):
        columns = element.split("\t")
        if ((columns[0].find('#') == -1) & (len(columns) >= 4)):
            # pdb.set_trace()

            product_id = str(columns[0])
            division_id = str(columns[1])
            division_description = str(columns[2])
            department_number = str(columns[3])
            department_description = str(columns[4])
            yield (product_id + "," + division_id + "," + division_description + "," + department_number + "," + department_description)

p = beam.Pipeline(options=pipeline_options)

# Apply a ParDo to the PCollection "words" to compute lengths for each word.
data = p | 'Load Data' >> beam.io.ReadFromText('gs://feeddata-test-konos-1/classifications.tab')
# data = p | beam.io.ReadFromText('data-small-class.tab')

data_filtered = data | 'Process Classifications file' >> beam.ParDo(filterLines())

# Output generation
# data_calc = data_filtered | beam.io.WriteToText('output/output-classification-' + str(currentTime) + '.csv')
data_calc = data_filtered | 'Write to output' >> beam.io.WriteToText('gs://feeddata-test-konos-1/classifications-processed.csv')

result = p.run()
result.wait_until_finish()